lab 1
=====

1. execute code to start dbms
```sh
docker-compose rm -f ; docker-compose up --build
```
log into adminer with `root`/`$MARIADB_ROOT_PASSWORD` and launch code in `./hw_1.sql`

2. True or false? Explain or give a counterexample:
    1. true, we can use operator `∩`(intersection) or add an condition to shorten the output
    2. true, you can replace them with projection and join

3. Let G be a relation over the schema (A, B) representing a graph. Express the following query in either SQL or the relational algebra:
    1. ```sql
        SELECT id
        FROM nodes
        WHERE 
            not(
                id in (
                    SELECT DISTINCT parent
                    FROM relations
                )
            );
        ```
    
    2. ```sql
        SELECT N1.id, N2.id
        FROM nodes AS N1, nodes as N2
        WHERE (
            SELECT COUNT(id)
            FROM nodes as n
            WHERE reach(N1.id, n.id)
        ) + (
            SELECT COUNT(id)
            FROM nodes as n
            WHERE reach(N2.id, n.id)
        ) = (
            SELECT COUNT(id)
            FROM nodes as n
        );
        ```
    
    3. ```sql
        SELECT N1.id, N2.id
        FROM nodes AS N1, nodes as N2
        WHERE (
            reach(N1.id, N2.id)
        ) OR EXISTS (
            SELECT id
            FROM nodes as n
            WHERE reach(N1.id, n.id) AND reach(n.id, N2.id)
        ) OR EXISTS (
            SELECT id
            FROM nodes as n1, nodes as n2
            WHERE reach(N1.id, n1.id) AND reach(n2.id, n2.id) AND reach(n2.id, N2.id)
        ) OR EXISTS (
            SELECT id
            FROM nodes as n1, nodes as n2, nodes as n3
            WHERE reach(N1.id, n1.id) AND reach(n2.id, n2.id) AND reach(n2.id, n3.id) AND reach(n3.id, N2.id)
        ) OR EXISTS (
            SELECT id
            FROM nodes as n1, nodes as n2, nodes as n3, nodes as n4
            WHERE reach(N1.id, n1.id) AND reach(n2.id, n2.id) AND reach(n2.id, n3.id) AND reach(n3.id, n4.id) and reach(n4.id, N2.id)
        );
        ```


