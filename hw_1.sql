DROP DATABASE lab_1;
CREATE DATABASE lab_1;
USE lab_1;

CREATE TABLE flight(
    /* replace varchar to enum? */
    flight_nr varchar(255) NOT NULL,
    dept varchar(255) NOT NULL,
    dest varchar(255) NOT NULL
);
CREATE TABLE plane(
    /* replace varchar to enum? */
    flight_nr varchar(255) NOT NULL,
    plane_nr varchar(255) NOT NULL
);
CREATE TABLE type(
    /* replace varchar to enum? */
    plane_nr varchar(255) NOT NULL,
    type varchar(255) NOT NULL
);

INSERT INTO flight(flight_nr, dept, dest)
VALUES
    ("AA89", "BRU", "LAX"),
    ("AA89", "BRU", "JFK"),
    ("UA04", "BRU", "LAX"),
    ("UA04", "BRU", "JFK"),
    ("AU77", "LAX", "BRU"),
    ("AU77", "JFK", "BRU")
;

INSERT INTO plane(flight_nr, plane_nr)
VALUES
    ("AA89", "P1078"),
    ("UA04", "P1078"),
    ("AU77", "P1078"),
    ("UA04", "P7878"),
    ("AU77", "P0001"),
    ("JK69", "UNFLY")
;

INSERT INTO type(plane_nr, type)
VALUES
    ("P1078", "747"),
    ("P7878", "98"),
    ("P0001", "69"),
    ("UNFLY", "69"),
    ("P1079", "748")
;

/* (a) Give all types of planes that are used on a connection from BRU. */
SELECT DISTINCT type
FROM flight
LEFT JOIN plane ON flight.flight_nr = plane.flight_nr
LEFT JOIN type ON plane.plane_nr = type.plane_nr
WHERE flight.dept = "BRU";

/* (b) Give all planes that are not involved in any flight. */
SELECT plane_nr
FROM plane
WHERE flight_nr NOT IN (SELECT flight_nr FROM flight);

/* (c) Give all types of planes that are involved in all flights. */
SELECT type
FROM type
WHERE (SELECT COUNT(DISTINCT flight_nr) FROM plane WHERE plane_nr = type.plane_nr) = (SELECT COUNT(DISTINCT flight_nr) FROM flight);

/* (d) Give all connections (i.e. departure-destination pairs) for which not all plane types are used. */
SELECT *
FROM flight
WHERE (SELECT COUNT(DISTINCT type.type) FROM plane LEFT JOIN type ON type.plane_nr = plane.plane_nr WHERE plane.flight_nr = flight_nr) <> (SELECT COUNT(DISTINCT type) FROM type);

SELECT DISTINCT dept, dest
FROM flight
WHERE (
    SELECT COUNT(DISTINCT type.type)
    FROM flight AS F
    LEFT JOIN plane ON F.flight_nr = plane.flight_nr
    LEFT JOIN type ON plane.plane_nr = type.plane_nr
    WHERE F.dept = dept AND F.dest = dest
) <> (SELECT COUNT(DISTINCT type) FROM type);

/* (e) Give all flight numbers that are used multiple times (e.g., (AA89,BRU,LAX) and (AA89,BRU,JFK) results in (AA89)). */
SELECT DISTINCT flight_nr
FROM flight
where (
    SELECT COUNT(F.flight_nr)
    FROM flight as F
    where F.flight_nr = flight_nr
) > 1;

/* (f) Give all pairs of different flight numbers that are used on the same connection (e.g., (AA89,BRU,LAX) and (UA04,BRU,LAX) results in (AA89,UA04)). */
SELECT
    F1.flight_nr,
    F2.flight_nr
FROM
    flight AS F1,
    flight AS F2
WHERE
    F1.dept = F2.dept
    AND F1.dest = F2.dest
    AND F1.flight_nr <> F2.flight_nr;

/* (g) Give all planes that have a plane type that is used at least once for every connection. */
SELECT plane_nr
FROM type
WHERE type IN (
    SELECT DISTINCT type.type
    FROM flight 
    LEFT JOIN plane AS P ON flight.flight_nr = P.flight_nr
    LEFT JOIN type ON P.plane_nr = type.plane_nr
);
